import dotenv from "dotenv";
import express from "express";
import connect from "./database";

dotenv.config();

const db_credentials = {
  port: process.env.DB_PORT as string,
  container_name: process.env.DB_CONTAINER_NAME as string,
};

const app = express();
const ports = {
  internal: process.env.PORT as string,
  external: process.env.EXTERNAL_PORT as string,
};

const connectionString = `mongodb://${db_credentials.container_name}:${db_credentials.port}/auth`;

const db = connect(connectionString);

db.once("open", () => {
  console.log("MongoDb is now connected!");

  app.listen(ports.internal, () => {
    console.log(`Example app listening at http://localhost:${ports.external}`);
  });
});

app.get("/", (req, res) => {
  res.send("Hello World!!!");
});

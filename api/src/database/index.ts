import mongoose from "mongoose";

const connect = (connectionString: string) => {
  mongoose
    .connect(connectionString, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .catch((e: Error) => {
      console.error("MongoDB connection error: ", e.message);
    });

  return mongoose.connection;
};
export default connect;

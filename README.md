This is a backend service for the <a href="https://gitlab.com/wiklz/test-react-native" target="_blank">mobile app</a> which I build in order to learn React Native.

### Steps to run locally:
1. `cd api && cp .env.sample .env && cd ../`
2. `docker compose up`
